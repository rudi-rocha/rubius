<?php

/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\Form;

use Rubius\AdminBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            'text',
            [
                'label' => 'rubiusAdmin.users.create.nameField',
                'attr' => [
                    'class' => 'form-control'
                ]
            ]
        )
            ->add(
                'username',
                'text',
                [
                    'label' => 'rubiusAdmin.users.create.usernameField',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            )
            ->add(
                'email',
                'text',
                [
                    'label' => 'rubiusAdmin.users.create.emailField',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            )
            ->add(
                'password',
                'repeated',
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.users.create.passwordField',
                    'first_name' => 'password',
                    'first_options' => [
                        'always_empty' => false,
                        'label' => 'rubiusAdmin.users.create.passwordField',
                        'attr' => [ 'class' => 'form-control']
                    ],
                    'second_name' => 'confirm',
                    'second_options' => [
                        'always_empty' => false,
                        'label' => 'rubiusAdmin.users.create.confirmField',
                        'attr' => [ 'class' => 'form-control']
                    ],
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'type' => 'password'
                ]
            )
            ->add('roles', 'role_select')

        ->add('submit', 'submit', ['attr' => ['class' => 'btn btn-success']]);
        ;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => false,
           'class' => User::class,
           'dataclass' => User::class,
            'translation_domain' => 'rubiusAdmin'
        ]);
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user_create';
    }
}