<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\DataTables;


use Doctrine\ORM\QueryBuilder;
use Rubius\DataTablesBundle\Library\ColumnObject;
use Rubius\DataTablesBundle\Library\DataTablesInterface;
use Rubius\DataTablesBundle\Strategy\DataTablesStrategy;

class ContentTypes extends DataTablesStrategy implements DataTablesInterface
{

    /**
     * @return QueryBuilder
     */
    public function setQueryBuilderObject()
    {
        return $this->setQueryBuilder(
            $this->getEntityManager()
                ->getRepository('RubiusAdminBundle:ContentType')
                ->getDataTableQuery($this->getColumns())
        );
    }

    /**
     * Build your column structure defining $this->setColumns($columnsArray)
     */
    public function defineColumns()
    {
        $columns = [];

        $column = new ColumnObject('rubiusAdmin.cms.contentType.index.gridId', 'id', 'contentTypes',  'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.cms.contentType.index.gridTitle', 'title', 'contentTypes',  'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.cms.contentType.index.gridDescription', 'description', 'contentTypes',  'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.cms.contentType.index.gridUrl', 'url', 'contentTypes',  'rubiusAdmin');
        $columns[] = $column;


        $column = new ColumnObject('rubiusAdmin.cms.contentType.index.gridActions', 'actions', null,  'rubiusAdmin');
        $columns[] = $column->setSortable(false);

        $this->setColumns($columns);
    }

    /**
     * Set Where statement to QueryBuilder
     */
    public function setWhereStatement()
    {
        if ($this->getRequest()->has('sSearch')){
            $this->getQueryBuilder()->andWhere(
                $this->getQueryBuilder()
                    ->expr()->like('contentTypes.title',sprintf("'%%%s%%'", $this->getRequest()->get('sSearch')))
            );
        }
    }

    protected function getFormattedData($rows)
    {
        $data = [];
        foreach ($rows as $row) {
            $dataRow = $this->mapAutomaticFields($row);
            $dataRow['actions'] = $this->getActions($row);

            $data[] = $dataRow;
        }

        return $data;
    }

    private function getActions($row)
    {
        return $this->getRenderer()->render(
            '@RubiusAdmin/CMS/content-type/partials/contentType-dt-actions.html.twig',
            [
                'row' => $row
            ]
        );
    }
}