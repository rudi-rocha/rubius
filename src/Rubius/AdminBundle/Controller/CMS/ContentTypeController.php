<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\Controller\CMS;


use Rubius\AdminBundle\Controller\DefaultController;
use Rubius\AdminBundle\Entity\ContentType;
use Rubius\DataTablesBundle\Library\DataTablesInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContentTypeController
 * @package Rubius\AdminBundle\Controller\CMS
 * @Security("has_role('ROLE_USER')")
 */
class ContentTypeController extends DefaultController
{
    /**
     * @Route(path="/", name="rubius_content_type_index")
     * @return Response
     */
    public function indexAction()
    {
        /** @var DataTablesInterface $table */
        $table = $this->get('datatable.factory')->getTable('contentTypes');

        return $this->render(
            'RubiusAdminBundle:CMS/content-type:index.html.twig',
            [
                'table' => $table->getDataTableObject($this->generateUrl('rubius_content_types_datatable'))
            ]
        );

    }

    /**
     * @Route(path="/getContentTypesData", name="rubius_content_types_datatable")
     */
    public function getContentTypesDataAction()
    {
        /** @var DataTablesInterface $table */
        $table = $this->get('datatable.factory')->getTable('contentTypes');

        return new JsonResponse(
            $table->getData()
        );
    }

    /**
     * @Route(path="/create", name="rubius_content_type_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('content_type');

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted())
        {
            /** @var ContentType $contentType */
            $contentType = $form->getData();
            $this->get('doctrine.orm.default_entity_manager')->persist($contentType);
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirect($this->generateUrl('rubius_content_type_index'));
        }
        return $this->render(
            '@RubiusAdmin/CMS/content-type/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/edit", name="rubius_content_type_edit")
     * @param Request $request
     * @param ContentType $contentType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request, ContentType $contentType)
    {
        $form = $this->createForm('content_type', $contentType);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted())
        {
            /** @var ContentType $contentType */
            $contentType = $form->getData();
            $contentType->uploadPrePersist();
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirect($this->generateUrl('rubius_content_type_index'));
        }
        return $this->render(
            '@RubiusAdmin/CMS/content-type/create.html.twig',
            [
                'form' => $form->createView(),
                'contentType' => $contentType
            ]
        );
    }

    /**
     * @Route(path="/{id}/delete", name="rubius_content_type_delete")
     * @param ContentType $contentType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteContentTypeAction(ContentType $contentType)
    {
        $this->get('doctrine.orm.default_entity_manager')->remove($contentType);
        $this->get('doctrine.orm.default_entity_manager')->flush();
        return $this->redirect($this->generateUrl('rubius_content_type_index'));
    }

}