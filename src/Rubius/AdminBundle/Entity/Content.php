<?php

namespace Rubius\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Content
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rubius\AdminBundle\Entity\Repository\ContentRepository")
 */
class Content
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="text", nullable=true)
     */
    private $intro;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255, nullable=true)
     */
    private $body;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", options={"default"=0})
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedDate", type="datetime", nullable=true)
     */
    private $publishedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="smallImage", type="string", length=255, nullable=true)
     */
    private $smallImage;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $smallImageObject;

    /**
     * @var string
     *
     * @ORM\Column(name="mediumImage", type="string", length=255, nullable=true)
     */
    private $mediumImage;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $mediumImageObject;

    /**
     * @var string
     *
     * @ORM\Column(name="bigImage", type="string", length=255, nullable=true)
     */
    private $bigImage;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $bigImageObject;

    /**
     * @var string
     *
     * @ORM\Column(name="metaTags", type="string", length=255, nullable=true)
     */
    private $metaTags;

    /**
     * @var string
     *
     * @ORM\Column(name="metaDescription", type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="forwardUrl", type="string", length=255, nullable=true)
     */
    private $forwardUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Rubius\AdminBundle\Entity\User", inversedBy="createdContents")
     * @ORM\JoinColumn(name="createdBy", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Rubius\AdminBundle\Entity\User", inversedBy="updatedContents")
     * @ORM\JoinColumn(name="updatedBy", referencedColumnName="id")
     */
    private $updatedBy;

    /**
     * @var ContentType
     * @ORM\ManyToOne(targetEntity="Rubius\AdminBundle\Entity\ContentType", inversedBy="contents")
     * @ORM\JoinColumn(name="content_type_id", referencedColumnName="id")
     */
    private $contentType;

    function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Content
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Content
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set intro
     *
     * @param string $intro
     *
     * @return Content
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro
     *
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Content
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Content
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set publishedDate
     *
     * @param \DateTime $publishedDate
     *
     * @return Content
     */
    public function setPublishedDate($publishedDate)
    {
        $this->publishedDate = $publishedDate ;//\Datetime::createFromFormat('d/m/Y', $publishedDate);

        return $this;
    }

    /**
     * Get publishedDate
     *
     * @return \DateTime
     */
    public function getPublishedDate()
    {
        return $this->publishedDate;
    }

    /**
     * Set smallImage
     *
     * @param string $smallImage
     *
     * @return Content
     */
    public function setSmallImage($smallImage)
    {
        $this->smallImage = $smallImage;

        return $this;
    }

    /**
     * Get smallImage URL
     *
     * @return string
     */
    public function getSmallImageUrl()
    {
        return sprintf("%s/%s",$this->getUploadDir(), $this->getSmallImage());
    }

    /**
     * Get smallImage
     *
     * @return string
     */
    public function getSmallImage()
    {
        return $this->smallImage;
    }

    /**
     * Set mediumImage
     *
     * @param string $mediumImage
     *
     * @return Content
     */
    public function setMediumImage($mediumImage)
    {
        $this->mediumImage = $mediumImage;

        return $this;
    }

    /**
     * Get mediumImage
     *
     * @return string
     */
    public function getMediumImage()
    {
        return $this->mediumImage;
    }

    /**
     * Get smallImage URL
     *
     * @return string
     */
    public function getMediumImageUrl()
    {
        return sprintf("%s/%s",$this->getUploadDir(), $this->getSmallImage());
    }

    /**
     * Set bigImage
     *
     * @param string $bigImage
     *
     * @return Content
     */
    public function setBigImage($bigImage)
    {
        $this->bigImage = $bigImage;

        return $this;
    }

    /**
     * Get smallImage URL
     *
     * @return string
     */
    public function getBigImageUrl()
    {
        return sprintf("%s/%s",$this->getUploadDir(), $this->getSmallImage());
    }

    /**
     * Get bigImage
     *
     * @return string
     */
    public function getBigImage()
    {
        return $this->bigImage;
    }

    /**
     * Set metaTags
     *
     * @param string $metaTags
     *
     * @return Content
     */
    public function setMetaTags($metaTags)
    {
        $this->metaTags = $metaTags;

        return $this;
    }

    /**
     * Get metaTags
     *
     * @return string
     */
    public function getMetaTags()
    {
        return $this->metaTags;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return Content
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set forwardUrl
     *
     * @param string $forwardUrl
     *
     * @return Content
     */
    public function setForwardUrl($forwardUrl)
    {
        $this->forwardUrl = $forwardUrl;

        return $this;
    }

    /**
     * Get forwardUrl
     *
     * @return string
     */
    public function getForwardUrl()
    {
        return $this->forwardUrl;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Content
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Content
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Content
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Rubius\AdminBundle\Entity\User $updatedBy
     *
     * @return Content
     */
    public function setUpdatedBy(\Rubius\AdminBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Rubius\AdminBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set contentType
     *
     * @param \Rubius\AdminBundle\Entity\ContentType $contentType
     *
     * @return Content
     */
    public function setContentType(\Rubius\AdminBundle\Entity\ContentType $contentType = null)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get contentType
     *
     * @return \Rubius\AdminBundle\Entity\ContentType
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @return mixed
     */
    public function getSmallImageObject()
    {
        return $this->smallImageObject;
    }

    /**
     * @param mixed $smallImageObject
     */
    public function setSmallImageObject($smallImageObject)
    {
        $this->smallImageObject = $smallImageObject;
    }

    /**
     * @return mixed
     */
    public function getMediumImageObject()
    {
        return $this->mediumImageObject;
    }

    /**
     * @param mixed $mediumImageObject
     */
    public function setMediumImageObject($mediumImageObject)
    {
        $this->mediumImageObject = $mediumImageObject;
    }

    /**
     * @return mixed
     */
    public function getBigImageObject()
    {
        return $this->bigImageObject;
    }

    /**
     * @param mixed $bigImageObject
     */
    public function setBigImageObject($bigImageObject)
    {
        $this->bigImageObject = $bigImageObject;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadPrePersist()
    {
        if (null !== $this->smallImageObject) {
            //remove old image id exists
            if (null !== $this->smallImage) {
                unlink($this->getUploadRootDir().'/'.$this->getSmallImage());
            }
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->smallImage = $filename . '.' . $this->smallImageObject->guessExtension();
            //move image to app directory
            $this->smallImageObject->move(
                $this->getUploadRootDir(),
                $this->smallImage
            );
            $this->smallImageObject = null;
        }

        if (null !== $this->mediumImageObject) {
            //remove old image id exists
            if (null !== $this->mediumImage) {
                unlink($this->getUploadRootDir().'/'.$this->getMediumImage());
            }
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->mediumImage = $filename . '.' . $this->mediumImageObject->guessExtension();
            //move image to app directory
            $this->mediumImageObject->move(
                $this->getUploadRootDir(),
                $this->mediumImage
            );
            $this->mediumImageObject = null;
        }

        if (null !== $this->bigImageObject) {
            //remove old image id exists
            if (null !== $this->bigImage) {
                unlink($this->getUploadRootDir().'/'.$this->getBigImage());
            }
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->bigImage = $filename . '.' . $this->bigImageObject->guessExtension();
            //move image to app directory
            $this->bigImageObject->move(
                $this->getUploadRootDir(),
                $this->bigImage
            );
            $this->bigImageObject = null;
        }

    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        if (null !== $this->smallImage) {
            unlink($this->getUploadRootDir().'/'.$this->getSmallImage());
        }

        if (null !== $this->mediumImage) {
            unlink($this->getUploadRootDir().'/'.$this->getMediumImage());
        }

        if (null !== $this->bigImage) {
            unlink($this->getUploadRootDir().'/'.$this->getBigImage());
        }


    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/content' ;
    }
}
